import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../servicios/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private usersService: UsersService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
  }

  login(form: NgForm): void{

    this.usersService.login(form.value).subscribe(response => {

      console.log(this.activatedRoute.url);
      this.usersService.setToken(response.token);
      this.router.navigateByUrl('/');

    });

  }

}
