import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectronicaDashboardComponent } from './electronica-dashboard.component';

describe('ElectronicaDashboardComponent', () => {
  let component: ElectronicaDashboardComponent;
  let fixture: ComponentFixture<ElectronicaDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElectronicaDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectronicaDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
