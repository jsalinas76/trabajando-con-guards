import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LibroDashboardComponent } from './libro-dashboard.component';

describe('LibroDashboardComponent', () => {
  let component: LibroDashboardComponent;
  let fixture: ComponentFixture<LibroDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LibroDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LibroDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
