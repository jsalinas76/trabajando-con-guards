import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LibroDashboardComponent } from './libro-dashboard/libro-dashboard.component';
import { ElectronicaDashboardComponent } from './electronica-dashboard/electronica-dashboard.component';
import { InicioDashboardComponent } from './inicio-dashboard/inicio-dashboard.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { UsersService } from './servicios/users.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LibroDashboardComponent,
    ElectronicaDashboardComponent,
    InicioDashboardComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [CookieService, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
