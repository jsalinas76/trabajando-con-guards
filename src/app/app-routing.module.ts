import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
import { ElectronicaDashboardComponent } from './electronica-dashboard/electronica-dashboard.component';
import { InicioDashboardComponent } from './inicio-dashboard/inicio-dashboard.component';
import { LibroDashboardComponent } from './libro-dashboard/libro-dashboard.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'libros', component: LibroDashboardComponent, canActivate: [AuthGuard]},
  {path: 'electronica', component: ElectronicaDashboardComponent, canActivate: [AuthGuard]},
  {path: '', component: InicioDashboardComponent},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
